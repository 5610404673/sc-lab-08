package abc;

import java.util.ArrayList;


public class TaxCalculator {
	
	
	public double Sum(ArrayList<Taxable> taxList) {
		double pay=0;
		for(Taxable tax:taxList){
			pay += tax.getTax();
		}
		
		return pay;
		
		
	}
	
}
