package abc;

public class Product implements Taxable{
	private double price;
	private double sum;
	private String name;
	
	public Product(String name,double price){
		this.name=name;
		this.price=price;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		sum = price*0.07;
		
		return sum;
	}

}
