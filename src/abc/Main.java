package abc;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TaxCalculator cal = new TaxCalculator();
		
		//person

		Person p1 = new Person("Belle",500000);
		Person p2 = new Person("Yong",50000);
		
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(p1);
		tax.add(p2);
		System.out.println(cal.Sum(tax));
		
		//Company
		
		Company c1 = new Company("Belle", 200000, 150000);
		Company c2 = new Company("Yong", 500000, 200000);
		
		ArrayList<Taxable> tax2 = new ArrayList<Taxable>();
		tax2.add(c1);
		tax2.add(c2);
		System.out.println(cal.Sum(tax2));
		
		//product
		Product d1 = new Product("BelleCom", 52000000);
		Product d2 = new Product("YongCom", 75000000);
		
		ArrayList<Taxable> tax3 = new ArrayList<Taxable>();
		tax3.add(d1);
		tax3.add(d2);
		System.out.println(cal.Sum(tax3));
	}

}
