package abc;

public class Person implements Taxable{
	private double income;
	private double sum;
	private String name;
	
	public Person(String name,double income){
		this.name=name;
		this.income=income;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		if(income<=300000){
			sum=income*0.05;
		}
		else if(income>300000){
			double temp = income-300000;
			sum = (temp*0.10)+15000;
		}
		
		return sum;
	}

}
