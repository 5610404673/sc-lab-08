package abc;

public interface Taxable {
	
	public double getTax();
}
