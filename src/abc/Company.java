package abc;

public class Company implements Taxable{
	private double income;
	private double outcome;
	private double sum;
	private String name;
	
	public Company(String name,double income,double outcome){
		this.name= name;
		this.income=income;
		this.outcome=outcome;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double temp = income-outcome;
		sum = temp*0.30;
		
		return sum;
	}

}
