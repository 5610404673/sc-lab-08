package lab08;

public class BankAccount implements Measurable {
				
	private int money;
	
	
	public BankAccount(int money) {
		this.money = money;
	}

	@Override
	public int getMeasure() {
		// TODO Auto-generated method stub
		return money;
	}
	
	public String toString(){
		return money+"";
	}
	
	
}
