package lab08;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//person
		Person p1 = new Person("Belle",158);
		Person p2 = new Person("Yong",162);
		
		Data d = new Data();
		
		ArrayList<Measurable> per = new ArrayList<Measurable>();
		per.add(p1);
		per.add(p2);
		System.out.println(d.Average(per));
	
		
		//Bank
		BankAccount b1 = new BankAccount(25000);
		BankAccount b2 = new BankAccount(450000);
		
		System.out.println(d.Minimumm(b1,b2));
		
		//Country
		Country c1 = new Country(5623000);
		Country c2 = new Country(78541000);
		
		System.out.println(d.Minimumm(c1,c2));
		
}
}
