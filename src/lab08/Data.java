package lab08;

import java.util.ArrayList;

public class Data {
	
	public double Average(ArrayList<Measurable> m){
		double sum=0;
		for(Measurable mea:m){
			sum += mea.getMeasure();
		}
		
		return (sum/m.size());
			
	}
	
	public Measurable Minimumm (Measurable m1, Measurable m2){
		if(m1.getMeasure()<m2.getMeasure()){
			return m1;
		}
		else{
		return m2;}
		
	}
}
