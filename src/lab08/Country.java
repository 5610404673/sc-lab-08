package lab08;

public class Country implements Measurable {
	private int area;

	public Country(int area){
		this.area=area;
	
	}

	@Override
	public int getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
	
	public String toString(){
		return area+"";
	}
}
